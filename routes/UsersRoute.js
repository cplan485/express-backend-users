const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/UsersController');

//  == This route will give us back all users: ==  //

router.get('/', controller.findAll);

//  == This route will give us back one user, it will be that with the id we are providing: ==  //

router.get('/:user_id', controller.findOne);

//  == This route allow us to create a user: ==  //

router.post('/new', controller.create);

//  == This route allow us to delete one user with the id we are providing: ==  //

router.post('/delete', controller.delete);

//  == This route allow us to update one user with the id we are providing ==  //

router.post('/update', controller.update);

module.exports = router;