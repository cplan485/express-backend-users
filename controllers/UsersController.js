const Users = require('../models/UsersModel');

class UsersController {
    //GET - FIND ALL
    async findAll(req, res){
        try{
            const users = await Users.find({});
            res.send(users);
        }
        catch(e){
            res.send({e})
        }
    }
    //GET - FIND ONE USER BY _ID
    async findOne(req ,res){
        let { user_id} = req.params;
        try{
            const user = await Users.findOne({_id:user_id});
            res.send(user);
        }
        catch(e){
            res.send({e})
        }

    }
    // POST - CREATE USER
    async create (req, res) {
        let { username } = req.body;
        try{
            const done = await Users.create({username});
            res.send(done)
        }
        catch(e){
            res.send({e})
        }
    }
    //POST - DELETE USER
    async delete (req, res){
        console.log('deleted!')
        let { username } = req.body;
        try{
            const removed = await Users.deleteOne({ username });
            res.send({removed});
        }
        catch(error){
            res.send({error});
        };
    }
    //POST - UPDATE USER

    async update (req, res){
        let { username, newUser } = req.body;
        try{
            const updated = await Users.updateOne(
                { username },{ username:newUser }
             );
            res.send({updated});
        }
        catch(error){
            res.send({error});
        };
    }

};
module.exports = new UsersController();