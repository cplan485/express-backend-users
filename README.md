# Back-end Users App
Node.js Express & MongoDB App to handle user functions with MVC (model, view, controller) pattern.

# How to Run It

Clone the repo, then

### `node index.js`

Or with nodemon (recommended)

### `nodemon`

You will need MongoDB & Mongoose. Official installation guide: https://docs.mongodb.com/manual/installation/. Then:
### `npm install express body-parser mongoose cors`

To Install express:
### `npm install express`
